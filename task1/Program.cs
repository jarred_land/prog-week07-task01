﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    class Program
    {
        static void Main(string[] args)
        {
            method();
        }

        static void method ()
        {
            Console.WriteLine("This is a method");
            Console.WriteLine("This second line is now printed to the screen");
        }
    }
}
